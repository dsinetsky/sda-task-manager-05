package ru.t1.dsinetsky.tm;

import ru.t1.dsinetsky.tm.constant.TerminalConst;

public class Application {

    public static void main(String[] args) {
        displayWelcome();
        run(args);
    }

    private static void displayHelp() {
        System.out.println("help - shows this message");
        System.out.println("version - shows program version");
        System.out.println("about - shows information about developer");
        System.exit(0);
    }

    private static void displayVersion() {
        System.out.println("Version: 1.5.0");
        System.exit(0);
    }

    private static void displayAbout() {
        System.out.println("Developer: Sinetsky Dmitry");
        System.out.println("Dev Email: dsinetsky@t1-consulting.ru");
        System.exit(0);
    }

    private static void displayWelcome() {
        System.out.println("Welcome to the task-manager_02!\n");
    }

    private static void displayError() {
        System.out.println("Invalid argument! Type \"help\" for list of arguments");
        System.exit(0);
    }

    private static void run(String[] args) {
        if (args.length < 1) {
            displayError();
            return;
        }
        String param = args[0];
        switch (param) {
            case (TerminalConst.CMD_HELP):
                displayHelp();
                break;
            case (TerminalConst.CMD_VERSION):
                displayVersion();
                break;
            case (TerminalConst.CMD_ABOUT):
                displayAbout();
                break;
            default:
                displayError();
                break;
        }
    }
}

